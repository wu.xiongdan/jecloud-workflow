/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.model;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import java.util.Map;

public class TaskCandidateDTO {
    /**
     * 节点名称
     */
    private String nodeName;
    /**
     * 节点key
     */
    private String nodeKey;
    /**
     * 流程名称
     */
    private String processName;
    /**
     * 流程key
     */
    private String processKey;
    /**
     * 表名
     */
    private String tableCode;
    /**
     * 主键
     */
    private String businessKey;
    /**
     * 功能编码
     */
    private String funcCode;
    /**
     * 待办人员ids
     */
    private String assigneeIds;
    /**
     * 待办人员名称s
     */
    private String assigneeNames;
    /**
     * 已办人员ids
     */
    private String hiAssigneeIds;
    /**
     * 已办人员名称s
     */
    private String hiAssigneeNames;
    /**
     * 提交人id
     */
    private String submitterId;
    /**
     * 提交人信息
     */
    private String lastSubmitterFlowInfo;
    /**
     * 提交人名称
     */
    private String submitterName;
    /**
     * 流程分类名称
     */
    private String processTypeName;
    /**
     * 流程分类code
     */
    private String processTypeCode;
    /**
     * 提交意见
     */
    private String comment;
    /**
     * 提交类型
     */
    private String commentTypeCode;
    /**
     * 提交类型名称
     */
    private String commentTypeName;
    /**
     * 流程部署id
     */
    private String pdId;
    /**
     * 流程实例id
     */
    private String piId;
    /**
     * DynaBean信息
     */
    private Map<String, Object> bean;


    public static TaskCandidateDTO build(Map<String, Object> bean, String tableCode, String beanId, String funcCode) {
        TaskCandidateDTO taskCandidateDTO = new TaskCandidateDTO();
        taskCandidateDTO.setBean(bean);
        taskCandidateDTO.setNodeName(getBeanValue(bean, "SY_CURRENTTASK"));
        taskCandidateDTO.setNodeKey(getBeanValue(bean, "SY_CURRENTTASK_KEYS"));
        taskCandidateDTO.setTableCode(tableCode);
        taskCandidateDTO.setBusinessKey(beanId);
        taskCandidateDTO.setFuncCode(funcCode);
        taskCandidateDTO.setAssigneeIds(getBeanValue(bean, "SY_PREAPPROVUSERS"));
        taskCandidateDTO.setAssigneeNames(getBeanValue(bean, "SY_PREAPPROVUSERNAMES"));
        taskCandidateDTO.setHiAssigneeIds(getBeanValue(bean, "SY_APPROVEDUSERS"));
        taskCandidateDTO.setHiAssigneeNames(getBeanValue(bean, "SY_APPROVEDUSERNAMES"));
        taskCandidateDTO.setSubmitterId(getBeanValue(bean, "SY_LASTFLOWUSERID"));
        taskCandidateDTO.setSubmitterName(getBeanValue(bean, "SY_LASTFLOWUSER"));
        taskCandidateDTO.setLastSubmitterFlowInfo(getBeanValue(bean, "SY_LASTFLOWINFO"));
        if (!Strings.isNullOrEmpty(getBeanValue(bean, "SY_LASTFLOWINFO"))) {
            JSONObject lastSubmit = JSONObject.parseObject(getBeanValue(bean, "SY_LASTFLOWINFO"));
            taskCandidateDTO.setComment(lastSubmit.getString("comment"));
            taskCandidateDTO.setCommentTypeCode(lastSubmit.getString("submitTypeCode"));
            taskCandidateDTO.setCommentTypeName(lastSubmit.getString("submitTypeName"));
        }
        taskCandidateDTO.setPdId(getBeanValue(bean, "SY_PDID"));
        taskCandidateDTO.setPiId(getBeanValue(bean, "SY_PIID"));
        return taskCandidateDTO;
    }

    private static String getBeanValue(Map<String, Object> bean, String value) {
        if (bean == null) {
            return "";
        }
        if (bean.get(value) == null) {
            return "";
        }
        return (String) bean.get(value);
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeKey() {
        return nodeKey;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public void setBean(Map<String, Object> bean) {
        this.bean = bean;
    }

    public void setNodeKey(String nodeKey) {
        this.nodeKey = nodeKey;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessKey() {
        return processKey;
    }

    public String getLastSubmitterFlowInfo() {
        return lastSubmitterFlowInfo;
    }

    public void setLastSubmitterFlowInfo(String lastSubmitterFlowInfo) {
        this.lastSubmitterFlowInfo = lastSubmitterFlowInfo;
    }

    public void setProcessKey(String processKey) {
        this.processKey = processKey;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getAssigneeIds() {
        return assigneeIds;
    }

    public void setAssigneeIds(String assigneeIds) {
        this.assigneeIds = assigneeIds;
    }

    public String getAssigneeNames() {
        return assigneeNames;
    }

    public void setAssigneeNames(String assigneeNames) {
        this.assigneeNames = assigneeNames;
    }

    public String getHiAssigneeIds() {
        return hiAssigneeIds;
    }

    public void setHiAssigneeIds(String hiAssigneeIds) {
        this.hiAssigneeIds = hiAssigneeIds;
    }

    public String getHiAssigneeNames() {
        return hiAssigneeNames;
    }

    public void setHiAssigneeNames(String hiAssigneeNames) {
        this.hiAssigneeNames = hiAssigneeNames;
    }

    public String getSubmitterId() {
        return submitterId;
    }

    public void setSubmitterId(String submitterId) {
        this.submitterId = submitterId;
    }

    public String getSubmitterName() {
        return submitterName;
    }

    public void setSubmitterName(String submitterName) {
        this.submitterName = submitterName;
    }

    public String getProcessTypeName() {
        return processTypeName;
    }

    public void setProcessTypeName(String processTypeName) {
        this.processTypeName = processTypeName;
    }

    public String getProcessTypeCode() {
        return processTypeCode;
    }

    public void setProcessTypeCode(String processTypeCode) {
        this.processTypeCode = processTypeCode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentTypeCode() {
        return commentTypeCode;
    }

    public void setCommentTypeCode(String commentTypeCode) {
        this.commentTypeCode = commentTypeCode;
    }

    public String getCommentTypeName() {
        return commentTypeName;
    }

    public void setCommentTypeName(String commentTypeName) {
        this.commentTypeName = commentTypeName;
    }

    public String getPdId() {
        return pdId;
    }

    public void setPdId(String pdId) {
        this.pdId = pdId;
    }

    public String getPiId() {
        return piId;
    }

    public void setPiId(String piId) {
        this.piId = piId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskCandidateDTO{");
        sb.append("nodeName='").append(nodeName).append('\'');
        sb.append(", nodeKey='").append(nodeKey).append('\'');
        sb.append(", processName='").append(processName).append('\'');
        sb.append(", processKey='").append(processKey).append('\'');
        sb.append(", tableCode='").append(tableCode).append('\'');
        sb.append(", businessKey='").append(businessKey).append('\'');
        sb.append(", funcCode='").append(funcCode).append('\'');
        sb.append(", assigneeIds='").append(assigneeIds).append('\'');
        sb.append(", assigneeNames='").append(assigneeNames).append('\'');
        sb.append(", hiAssigneeIds='").append(hiAssigneeIds).append('\'');
        sb.append(", hiAssigneeNames='").append(hiAssigneeNames).append('\'');
        sb.append(", submitterId='").append(submitterId).append('\'');
        sb.append(", lastSubmitterFlowInfo='").append(lastSubmitterFlowInfo).append('\'');
        sb.append(", submitterName='").append(submitterName).append('\'');
        sb.append(", processTypeName='").append(processTypeName).append('\'');
        sb.append(", processTypeCode='").append(processTypeCode).append('\'');
        sb.append(", comment='").append(comment).append('\'');
        sb.append(", commentTypeCode='").append(commentTypeCode).append('\'');
        sb.append(", commentTypeName='").append(commentTypeName).append('\'');
        sb.append(", pdId='").append(pdId).append('\'');
        sb.append(", piId='").append(piId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
