/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.model;

import com.google.common.base.Strings;

public class AccountVo {

    private static final long serialVersionUID = 1L;

    private String deptId;
    private String code;
    private String name;
    private String realUserId;
    private String phone;
    private String email;
    private String departmentId;
    private String departmentName;
    private String departmentCode;


    private AccountVo() {
    }

    private AccountVo(Builder builder) {
        this.deptId = builder.deptId;
        this.code = builder.code;
        this.name = builder.name;
        this.realUserId = builder.realUserId;
        this.phone = builder.phone;
        this.email = builder.email;
        this.departmentId = builder.departmentId;
        this.departmentName = builder.departmentName;
        this.departmentCode = builder.departmentCode;
    }

    public static class Builder {
        private String deptId;
        private String code;
        private String name;
        private String realUserId;
        private String phone;
        private String email;
        private String departmentId;
        private String departmentName;
        private String departmentCode;

        public Builder() {
        }

        public AccountVo build() {
            if (Strings.isNullOrEmpty(deptId)) {
                throw new IllegalArgumentException("参数 deptId 不可以为空");
            }
            if (Strings.isNullOrEmpty(code)) {
                throw new IllegalArgumentException("参数 code 不可以为空");
            }
            if (Strings.isNullOrEmpty(name)) {
                throw new IllegalArgumentException("参数 name 不可以为空");
            }
            if (Strings.isNullOrEmpty(realUserId)) {
                throw new IllegalArgumentException("参数 realUserId 不可以为空");
            }
            return new AccountVo(this);
        }

        public Builder deptId(String deptId) {
            this.deptId = deptId;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder realUserId(String realUserId) {
            this.realUserId = realUserId;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder departmentId(String departmentId) {
            this.departmentId = departmentId;
            return this;
        }

        public Builder departmentName(String departmentName) {
            this.departmentName = departmentName;
            return this;
        }

        public Builder departmentCode(String departmentCode) {
            this.departmentCode = departmentCode;
            return this;
        }
    }

    public String getDeptId() {
        return deptId;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getRealUserId() {
        return realUserId;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }
}
