/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.controller.procerss;

import com.google.common.base.Strings;
import com.je.bpm.engine.ActivitiException;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.workflow.service.button.ButtonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 流程信息控制层
 */

@RestController
@RequestMapping(value = "/je/workflow/button")
public class ButtonController extends AbstractPlatformController {

    @Autowired
    private ButtonService buttonService;

    /**
     * 获取流程按钮操作参数详情
     */
    @RequestMapping(value = "/getParams", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getParams(BaseMethodArgument param, HttpServletRequest request) {
        String operationId = getStringParameter(request, "operationId");
        if (Strings.isNullOrEmpty(operationId)) {
            throw new ActivitiException("operationId为空！");
        }
        return BaseRespResult.successResult(buttonService.getParams(operationId));
    }

    /**
     * 执行流程按钮操作
     */
    @RequestMapping(value = "/operate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult operate(BaseMethodArgument param, HttpServletRequest request) {
        //---------------通用参数---------------------------
        String operationId = getStringParameter(request, "operationId");
        String prod = getStringParameter(request, "prod");
        String funcCode = getStringParameter(request, "funcCode");
        String pdid = getStringParameter(request, "pdid");
        String beanId = getStringParameter(request, "beanId");
        String tableCode = getStringParameter(request, "tableCode");
        String funcId = getStringParameter(request, "funcId");
        //--------------节点人员信息参数--------------------------
        String assignee = getStringParameter(request, "assignee");
        buttonService.addCommonUser(assignee);
        //--------------自定义参数--------------------------
        Map<String, Object> operationCustomerParam = buttonService.buildOperationCustomerParam(operationId, request);
        Map<String, Object> result = buttonService.operate(operationId, prod, funcCode, pdid, beanId, tableCode, funcId, operationCustomerParam);
        return BaseRespResult.successResult(result);
    }

}
