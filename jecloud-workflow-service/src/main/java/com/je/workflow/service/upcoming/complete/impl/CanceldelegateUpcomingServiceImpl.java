/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete.impl;

import com.google.common.base.Strings;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 取消委托
 */
@Service
public class CanceldelegateUpcomingServiceImpl extends AbstractCompleteUpcomingService {

    @Override
    public void complete(String taskId, String beanId, String comment, String piid, String nodeId, Map<String, String> params, Map<String, Object> bean) {
        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder().eq("TASK_ACTIVITI_TASK_ID", taskId);
        List<DynaBean> dynaBeans = metaService.select("JE_WORKFLOW_RN_TASK",
                conditionsWrapper);
        String userId = "";
        if (dynaBeans.size() > 0) {
            userId = dynaBeans.get(0).getStr("ASSIGNEE_ID");
        }
        String transferUserId = params.get("transferUserId");
        DynaBean dynaBean = finishTask(taskId);
        insertTask(dynaBean, transferUserId, SubmitTypeEnum.CANCELDELEGATE, "取消委托");
        if (!Strings.isNullOrEmpty(userId)) {
            String associationId = workFlowUserService.getAssociationIdById(userId);
            pushService.pushRefresh(associationId);
        }

    }

}
