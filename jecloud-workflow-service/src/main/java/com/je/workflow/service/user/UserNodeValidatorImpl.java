/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user;

import com.je.bpm.runtime.shared.identity.UserNodeValidator;
import com.je.common.base.service.rpc.DataSecretRpcServive;
import com.je.workflow.exception.ClearanceMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserNodeValidatorImpl implements UserNodeValidator {

    @Autowired
    private DataSecretRpcServive dataSecretRpcServive;
    @Autowired
    private WorkFlowUserService userService;

    private static final String ERROR_MESSAGE = "提交节点处理人(%s)和当前节点(%s)密级信息不匹配，请重新提交！";

    /**
     * 校验用户和节点密级是否符合
     *
     * @param userIds
     * @param nodeName
     * @param nodeClearance
     */
    public void validatePersonnelClearance(List<String> userIds, String nodeName, String nodeClearance) {
        for (String userId : userIds) {
            boolean b = dataSecretRpcServive.validUserAndFlowNodeSecret(userId, nodeClearance);
            if (!b) {
                throw new ClearanceMismatchException(String.format(ERROR_MESSAGE, userService.getUserNameById(userId), nodeName));
            }
        }
    }
}
