/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete.impl;

import com.google.common.base.Strings;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 已阅
 */
@Service
public class PassroundreadUpcomingServiceImpl extends AbstractCompleteUpcomingService {

    @Override
    public void complete(String taskId, String beanId, String comment, String piid, String nodeId, Map<String, String> params, Map<String, Object> bean) {
        if(Strings.isNullOrEmpty(piid)){
            piid = params.get("piid");
        }
        finishTask(piid, Authentication.getAuthenticatedUser().getDeptId());
    }

    @Override
    public DynaBean finishTask(String piid, String assigneeId) {
        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder().eq("TASK_PIID", piid)
                .eq("TASK_CIRCULATION", "1");
        conditionsWrapper.eq("ASSIGNEE_ID", assigneeId);
        List<DynaBean> dynaBeans = metaService.select("JE_WORKFLOW_RN_TASK",
                conditionsWrapper);
        if (dynaBeans == null) {
            return null;
        }
        for (DynaBean dynaBean : dynaBeans) {
            dynaBean.setStr("TASK_HANDLE", "1");
            metaService.update(dynaBean);
            //如果有待办，直接删除，避免重复数据，查询待办会很慢
            List<DynaBean> list = metaService.select("JE_WORKFLOW_RN_TASK",
                    ConditionsWrapper.builder().eq("ASSIGNEE_ID", dynaBean.getStr("ASSIGNEE_ID"))
                            .eq("TASK_PIID", dynaBean.getStr("TASK_PIID"))
                            .eq("TASK_HANDLE", "0"));
            if (list.size() > 0) {
                metaService.executeSql(String.format("DELETE FROM JE_WORKFLOW_RN_TASK WHERE ASSIGNEE_ID='%s' and TASK_PIID='%s'" +
                                " and TASK_HANDLE='1'", dynaBean.getStr("ASSIGNEE_ID"),
                        dynaBean.getStr("TASK_PIID")));
            }
        }
        if (dynaBeans.size() == 0) {
            return null;
        }
        return dynaBeans.get(0);
    }

}
