/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.upcoming.complete.impl;

import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 撤销
 */
@Service
public class CancelUpcomingServiceImpl extends AbstractCompleteUpcomingService {

    @Override
    public void complete(String taskId, String beanId, String comment, String piid, String nodeId, Map<String, String> params, Map<String, Object> bean) {
        List<DynaBean> executions = metaService.select("je_workflow_rn_execution",
                ConditionsWrapper.builder().eq("BUSINESS_KEY", beanId));
        if (executions.size() == 0) {
            return;
        }
        List<String> executionIds = new ArrayList<>();
        for (DynaBean dynaBean : executions) {
            executionIds.add(dynaBean.getPkValue());
        }
        List<DynaBean> list = metaService.select(ConditionsWrapper.builder().table("je_workflow_rn_task").
                in("JE_WORKFLOW_RN_EXECUTION_ID", executionIds));
        if (list.size() == 0) {
            return;
        }

        for (DynaBean dynaBean : list) {
            metaService.delete(ConditionsWrapper.builder().table("je_workflow_rn_task").
                    eq("JE_WORKFLOW_RN_TASK_ID", dynaBean.getPkValue()));
        }

        metaService.delete("je_workflow_rn_execution", ConditionsWrapper.builder()
                .in("je_workflow_rn_execution_id", executionIds));

    }

}
