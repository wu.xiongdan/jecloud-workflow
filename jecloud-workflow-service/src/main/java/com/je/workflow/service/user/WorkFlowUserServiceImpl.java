/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.user;

import com.je.common.base.DynaBean;
import com.je.rbac.rpc.MetaRbacRpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WorkFlowUserServiceImpl implements WorkFlowUserService {

    @Autowired
    MetaRbacRpcServiceImpl metaRbacRpcService;

    private Map<String, Map<String, String>> userInfos = new HashMap<>();

    @Override
    public String getUserNameById(String userId) {
        if (userInfos.get(userId) != null) {
            return userInfos.get(userId).get("name");
        }
        DynaBean accountDept = getUserInfoById(userId);
        return accountDept.getStr("ACCOUNT_NAME");
    }

    @Override
    public String getUserDeptNameById(String userId) {
        if (userInfos.get(userId) != null) {
            return userInfos.get(userId).get("deptName");
        }
        DynaBean accountDept = getUserInfoById(userId);
        return accountDept.getStr("ACCOUNTDEPT_DEPT_NAME");
    }

    @Override
    public String getUserDeptIdById(String userId) {
        if (userInfos.get(userId) != null) {
            return userInfos.get(userId).get("deptId");
        }
        DynaBean accountDept = getUserInfoById(userId);
        return accountDept.getStr("ACCOUNTDEPT_DEPT_ID");
    }

    @Override
    public String getGender(String userId) {
        if (userInfos.get(userId) != null) {
            return userInfos.get(userId).get("gender");
        }
        DynaBean accountDept = getUserInfoById(userId);
        return accountDept.getStr("ACCOUNT_AVATAR");
    }

    @Override
    public String getUserPhotoById(String userId) {
        if (userInfos.get(userId) != null) {
            return userInfos.get(userId).get("photo");
        }
        DynaBean accountDept = getUserInfoById(userId);
        return accountDept.getStr("ACCOUNT_AVATAR");
    }

    @Override
    public String getAssociationIdById(String userId) {
        if (userInfos.get(userId) != null) {
            return userInfos.get(userId).get("associationId");
        }
        DynaBean accountDept = getUserInfoById(userId);
        return accountDept.getStr("USER_ASSOCIATION_ID");
    }

    private DynaBean getUserInfoById(String userId) {
        DynaBean accountDept = metaRbacRpcService.selectOneByPk("JE_RBAC_VACCOUNTDEPT", userId);
        if (accountDept == null) {
            accountDept = new DynaBean();
            accountDept.setStr("ACCOUNT_NAME", "");
            accountDept.setStr("ACCOUNT_AVATAR", "");
            accountDept.setStr("USER_ASSOCIATION_ID", "");
            accountDept.setStr("ACCOUNTDEPT_DEPT_NAME", "");
            accountDept.setStr("ACCOUNT_SEX", "MAN");
            accountDept.setStr("ACCOUNTDEPT_DEPT_ID", "");
        }
        Map<String, String> userInfo = new HashMap<>();
        userInfo.put("name", accountDept.getStr("ACCOUNT_NAME"));
        userInfo.put("photo", accountDept.getStr("ACCOUNT_AVATAR"));
        userInfo.put("associationId", accountDept.getStr("USER_ASSOCIATION_ID"));
        userInfo.put("deptName", accountDept.getStr("ACCOUNTDEPT_DEPT_NAME"));
        userInfo.put("deptId", accountDept.getStr("ACCOUNTDEPT_DEPT_ID"));
        userInfo.put("gender", accountDept.getStr("ACCOUNT_SEX"));
        userInfos.put(userId, userInfo);
        return accountDept;
    }


    @Override
    public Map<String, Object> getUserInfoByUserId(String userId) {
        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("name", getUserNameById(userId));
        userInfo.put("id", userId);
        userInfo.put("gender", getGender(userId));
        return userInfo;
    }
}
