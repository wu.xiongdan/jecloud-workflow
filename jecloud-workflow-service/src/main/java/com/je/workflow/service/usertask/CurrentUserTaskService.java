/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.usertask;

import com.je.common.base.result.BaseRespResult;
import com.je.common.base.workflow.vo.CirculationHistoryVo;

import java.util.List;
import java.util.Map;

public interface CurrentUserTaskService {

    public Map<String, Object> getCurrentUserTaskList(CurrentUserTaskEnum findCurrentTaskEnum, CurrentUserParam upcomingParam);

    List<Map<String, String>> getInitiateList(String sort, String name);

    public Map<String, Long> getBadges(CurrentUserParam upcomingParam);

    public Boolean collect(String piid, String status);

    public Boolean delay(String id, String status);

    public List<CirculationHistoryVo> getCirculationHistory(String beanId);

    /**
     * 设为已读
     * @param pkId 主键
     * @return
     */
    BaseRespResult setAsRead(String pkId);
    /**
     * 获取流程代办角标
     * @return
     */
    BaseRespResult getRreApprovNum(String userId, String deptId);
}
